import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Input, Output, EventEmitter } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Headers } from '@angular/http';
import { AbstractControl } from '@angular/forms';

import { DataService } from './services/data.service';
import { UserService } from './services/user.service';

import { AuthGuard } from './auth.guard';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SettingsComponent } from './components/settings/settings.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ListComponent } from './components/list/list.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SensorReadingComponent } from './components/sensor-reading/sensor-reading.component';
import { InvalidUrlComponent } from './components/invalid-url/invalid-url.component';
import { ConnectionIndicatorComponent } from './components/connection-indicator/connection-indicator.component';
import { DragComponent } from './components/drag/drag.component';

import { DragulaModule, DragulaService } from 'ng2-dragula';

const appRoutes: Routes = [
  {path: 'dashboard/:uid', component: DashboardComponent},
  {path: 'dashboard', component: DashboardComponent},
  // {path:'list', canActivate: [AuthGuard], component:ListComponent},
  {path: 'list', canActivate: [AuthGuard], component:ListComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'invalid-url', component: InvalidUrlComponent},
  {path: 'drag', component: DragComponent},
  {path: '**', redirectTo: '/dashboard'}
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SettingsComponent,
    NavbarComponent,
    ListComponent,
    LoginComponent,
    RegisterComponent,
    SensorReadingComponent,
    InvalidUrlComponent,
    ConnectionIndicatorComponent,
    DragComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    DragulaModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    DataService,
    UserService,
    AuthGuard
    // DragulaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
