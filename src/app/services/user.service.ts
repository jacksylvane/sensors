import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers  } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

  public isUserLoggedIn;
  private username;

  constructor(private router: Router, public http: HttpClient) {
    this.isUserLoggedIn = false;
  }

  setUserLoggedIn() {
    this.isUserLoggedIn = true;
    // console.log(this.isUserLoggedIn);
    return this.http.get('http://172.22.20.121/api/user/login');
  }

  getUserLoggedIn() {
    return this.isUserLoggedIn;
  }

  // Login validation
  validateLogIn(user) {
  if ((user.username === undefined || user.username === '') || (user.password === undefined || user.password === '')) {
    return false;
  } else {
    return true;
  }
}
validateCredentials(user) {
  const body = {'User_Username': user.username, 'User_Password': user.password};
  console.log(body);
  const options = { headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8' })};
  return new Promise ((resolve, reject) => {
    this.http.post('http://172.22.20.121/api/user/login', body, options)
    .subscribe(data => {
      console.log(data);
      if (data === 'Logged in succcesful!' ) {
        resolve(true);
      }
      resolve(false);
    }, error => {
      console.log('Problem with authorization');
    });
  }
);
}

// Check if username is already registered
validateNewCredentials(newuser) {
  if (newuser.username === 'admin') {
    return false;
  } else {
     return true; }
}

onLogoutClick() {
  this.isUserLoggedIn = false;
  // console.log(this.isUserLoggedIn);
  this.router.navigate(['/']);
  }
}
