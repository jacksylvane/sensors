import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { Http, Headers  } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Sensor } from '../components/Sensor';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {
  data: Observable<Array<Sensor[]>>;
  data2: any;
  sensors: Observable<Array<Sensor[]>>;
  dashboard: Observable<Object>;
  sensorsF: any;
  countOfTimeouts: number;
  // sensorsDashboard:Observable<Array<SensorDashboard[]>>;
  constructor(public http: HttpClient) {
  // Fake data for testing purposes
  this.countOfTimeouts = 0;
  this.sensorsF = [
    {
      sensor_Name: 'Tank 1',
      sensor_Reading_CreateDate: '2016-09-10T16:27:43',
      sensor_Reading_Dec: 7,
      sensor_Reading_Int: 10,
      sensor_Reading_Txt: 'smallSize',
      sensor_Reading_UID: 'd9e4f7ef-5a10-4cf9-bee5-dce60a74af11',
      sensor_UID: '58949908-e076-470d-9c08-c64769f5a4e3'
    },
    {
      sensor_Name: 'Tank 2',
      sensor_Reading_CreateDate: '2016-09-10T16:27:43',
      sensor_Reading_Dec: 14.2,
      sensor_Reading_Int: 7,
      sensor_Reading_Txt: 'mediumSize',
      sensor_Reading_UID: 'd9e4f7ef-5a10-4cf9-bee5-dce60a74af11',
      sensor_UID: '58949908-e076-470d-9c08-c64769f5a4e3'
    },
    {
      sensor_Name: 'Tank 3',
      sensor_Reading_CreateDate: '2016-09-10T16:27:43',
      sensor_Reading_Dec: 25,
      sensor_Reading_Int: 8,
      sensor_Reading_Txt: 'smallSize',
      sensor_Reading_UID: 'd9e4f7ef-5a10-4cf9-bee5-dce60a74af11',
      sensor_UID: '58949908-e076-470d-9c08-c64769f5a4e3'
    },
    {
      sensor_Name: 'Tank 4',
      sensor_Reading_CreateDate: '2016-09-10T16:27:43',
      sensor_Reading_Dec: 25.7,
      sensor_Reading_Int: 1,
      sensor_Reading_Txt: 'smallSize',
      sensor_Reading_UID: 'd9e4f7ef-5a10-4cf9-bee5-dce60a74af11',
      sensor_UID: '58949908-e076-470d-9c08-c64769f5a4e3'
    }
  ];
}

// Pushing a variable number of readings for testing purposes
getFakeSensorsReadings() {
  for (let i = 0; i < 1; i++) {
    const fakeLateSensorReading = {
      sensor_Name: 'Tank' + ' ' + i,
      sensor_Reading_CreateDate: '2016-09-10T16:27:43',
      sensor_Reading_Dec: (i / 2.0 + 0.3),
      sensor_Reading_Int: 2,
      sensor_Reading_Txt: 'smallSize',
      sensor_Reading_UID: 'd9e4f7ef-5a10-4cf9-bee5-dce60a74af11',
      sensor_UID: '58949908-e076-470d-9c08-c64769f5a4e3'
    };
    this.sensorsF.push(fakeLateSensorReading);
  }
  this.sensorsF.sort(function (a, b) {
    return a.sensor_Reading_Int - b.sensor_Reading_Int;
  });
  // console.log(this.sensorsF);

  return this.sensorsF;
}

getSensorDashboard(uid) {
  // return this.http.get('http://172.22.20.119/api/dashboard/'+ uid)
  return this.http.get('http://localhost:60000/api/reading/getall')
  .catch((err) => {
    this.countOfTimeouts++;
    return Observable.throw(err);
  });
}

getCountOfTimeouts() {
  return this.countOfTimeouts;
}
initialiseCountOfTimeouts() {
  this.countOfTimeouts = 0;
}
getSensors() {
  return this.http.get('http://localhost:60000/api/sensor/getall');
}
// getDashboard(){
//   const body = {"Dashboard_UID":"58949908-e076-470d-9c08-c64769f5a4e3"};
//   // console.log(body);
//   var options = { headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8', 'Access-Control-Allow-Origin': '*'})};
//    return this.http.post('http://172.22.20.119/api/dashboard/specific', body, options)
//    .subscribe(data => {
//      console.log(data);
//    }, error => {
//      console.log("problem with something");
//    });
// }

editSensor(sensor) {
  const body = {'sensor_UID': sensor.sensor_UID, 'sensor_ID': sensor.sensor_ID, 'sensor_Name': sensor.sensor_Name};
  console.log(body);
  const options = { headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8' })};
  this.http.post('http://172.22.20.119/api/sensor/update', body, options)
  .subscribe(data => {
    // console.log(data);
  }, error => {
    console.log('problem with something');
  });
}
}
