import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router, public user:UserService) {
    }

  ngOnInit() {
      this.user.getUserLoggedIn();
      if(localStorage.getItem("currentUser") == "loggedIn"){
        var currentUser = localStorage.getItem("currentUser");
        console.log(currentUser);
        this.user.setUserLoggedIn();
      }
  }
  onLogoutClick(){
    this.user.isUserLoggedIn = false;
    localStorage.removeItem('currentUser');
    console.log("You are logged-out");
    this.router.navigate(['/login']);
    return false;
  }
}
