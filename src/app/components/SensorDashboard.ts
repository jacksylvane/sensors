export interface SensorDashboard {
  sensor_UID: string,
  sensor_ID: string,
  sensor_Name: string,
  sensor_TypeID: number,
  sensor_StatusID: number,
  sensor_CreateDate: string
}
