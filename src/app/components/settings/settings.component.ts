import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.sass']
})
export class SettingsComponent implements OnInit {
  currentTheme:boolean =true;//true == white theme


  toggleTheme(){
    console.log(this.currentTheme);
    if(this.currentTheme){
      document.body.style.background = "#282527";
      // var sensorCard = document.getElemtById("#settings");
      // sensorCard.style.display = "none";
      console.log("Theme changed to dark!");
    } else {
      document.body.style.background = "linear-gradient(to right, #1487D4, #31AA70)";
      console.log("Theme changed to white!");
    }
    this.currentTheme = !this.currentTheme;

    console.log(this.currentTheme);
  }
  constructor() { }

  ngOnInit() {
  }

}
