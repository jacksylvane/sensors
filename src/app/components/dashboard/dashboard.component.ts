import { Component, OnInit } from '@angular/core';
import { Sensor } from '../Sensor';
import { DataService } from '../../services/data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DragulaModule, DragulaService } from 'ng2-dragula';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})

export class DashboardComponent implements OnInit {
  data: any[] = [];
  fakeData: any = [];
  sensors: any = [];
  dashboard: any;
  sensorsF: any = [];
  dimension = '1000px';
  senzors: any = [];
  remainder: any;
  done: any = false;
  maxNumberOfFails: number;
  showConnectionError = false;
  activatedRoute: ActivatedRoute;

  // UID for dashboard from URL request
  uid: string;

// sensors:Sensor[];
constructor(public dataService: DataService, activatedRoute: ActivatedRoute,
  private router: Router, private dragulaService: DragulaService) {
  dragulaService.setOptions('whatever', {
    // this needs to be checked. should return element back to its place
    revertOnSpill: true,
    moves: function (el, container, handle) {
      return handle.className === 'handle';
    }
  });
  this.maxNumberOfFails = 2;
  this.activatedRoute = activatedRoute;

  this.sensorsF = this.dataService.getFakeSensorsReadings();
  for (let i = 0; i < this.sensorsF.length; i++) {
    this.sensorsF[i].sensor_Reading_CreateDate = new Date(this.sensorsF[i].sensor_Reading_CreateDate);
  }
  this.getSensorDimension();
}

getSize(size) {
  return size;
}
/* Calculates rows and columns */
getSensorDimension() {
  const numSensors = this.sensorsF.length;
  let rows;
  let columns;
  const sqrtOfSensors = Math.sqrt(numSensors);
  if (Math.ceil(sqrtOfSensors) === sqrtOfSensors) {
    columns = sqrtOfSensors;
    rows = numSensors / columns;
  } else {
    columns = Math.ceil(sqrtOfSensors);
    rows = Math.round(numSensors / (columns - 1));
  }

  const htmlStyles = window.getComputedStyle(document.querySelector('html'));
  const rowNum = parseInt(htmlStyles.getPropertyValue('--rowNum'), 10 );
  document.documentElement.style.setProperty('--rowNum', rows);
  const colNum = parseInt(htmlStyles.getPropertyValue('--colNum'), 10);
  document.documentElement.style.setProperty('--colNum', columns);

  console.log('Columns: ' + columns + ' Rows: ' + rows);

  const height = window.innerHeight;
  const gridHeight = height / rows;
  const gridHeightString = gridHeight.toString();

  const fontSize = gridHeight / 12;
  let fontSizeString = fontSize.toString();
  fontSizeString = fontSizeString.concat('px');

  const fontSizeTemperature = (gridHeight / 5).toFixed(0);
  console.log(fontSizeTemperature);
  let fontSizeTemperatureString = fontSizeTemperature.toString();
  fontSizeTemperatureString = fontSizeTemperatureString.concat('px');

  const margin = gridHeight / 50;
  let marginString = margin.toString();
  marginString = marginString.concat('px');

  const border = gridHeight / 50;
  let borderString = border.toString();
  borderString = borderString.concat('px');

  document.documentElement.style.setProperty('--border', borderString);
  document.documentElement.style.setProperty('--gridHeight', gridHeightString);
  document.documentElement.style.setProperty('--fontSize', fontSizeString);
  document.documentElement.style.setProperty('--fontSizeTemperature', fontSizeTemperatureString);
  document.documentElement.style.setProperty('--margin', marginString);
}

gettingSensorData(uid) {
  if (this.dataService.getCountOfTimeouts() > this.maxNumberOfFails) {
    console.log(' NO CONNECTION ');
    this.showConnectionError = true;
  }
  this.dataService.getSensorDashboard(uid).subscribe(sensors => {
    this.dataService.initialiseCountOfTimeouts();
    this.showConnectionError = false;
    if (sensors === 'Nothing here') {
      this.router.navigate(['/invalid-url']);
      return;
    }
    this.sensors = sensors;
    for (let i = 0; i < this.sensors.length; i++) {
      this.sensors[i].sensor_Reading_CreateDate = new Date(this.sensors[i].sensor_Reading_CreateDate);
    }

    this.getSensorDimension();
    console.log(this.sensors);
  });
}

  ngOnInit() {
    this.dragulaService
      .drop
      .subscribe(value => {
        // TO-DO : Create a for loop here and save array.
        // now it gives the card on first position
        console.log(value[2].children[0].attributes[3].value);
      });
  }
    // this.activatedRoute.params.subscribe(
    //   (params: Params) => {
    //     let uid = params.uid;
    //     this.gettingSensorData(uid);
    //
    //     // setInterval(()=>{
    //     //   this.gettingSensorData(uid);
    //     // }, 2000);
    //
    //   },
    //   err => { console.log("We have encountered a problem with getting data");
    // }
    // );
  }

