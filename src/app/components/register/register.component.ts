import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  username: string;
  email: string
  password: string;
  secondpassword: string;

  constructor(private router:Router, private userService:UserService) { }
  registerUser(f){
    if (f.invalid) {
      console.log("Form is invalid");
      return;
    }
    const newuser = {
      username: this.username,
      email: this.email,
      password: this.password,
      secondpassword: this.secondpassword
    }

    console.log("username:  " + this.username);
    console.log("email:  " + this.email);
    console.log("password:  " + this.password);
    console.log("2nd password:  " + this.secondpassword);


    if (!this.userService.validateNewCredentials(newuser)) {
      console.log("You cannot be admin. Try again.");
    }  else {
      console.log("Welcome, your account has been created");
      this.userService.setUserLoggedIn();
      this.router.navigate(['/list']);
    }
  }
  ngOnInit() {
  }

}
