import { Component, OnInit } from '@angular/core';
import { Sensor } from '../Sensor';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataService } from '../../services/data.service';
import { FormsModule } from '@angular/forms';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {
  sensors:any;
  sensor = {
    sensor_Name: ''
  };
  constructor(public dataService:DataService, private user: UserService) {
    this.dataService.getSensors().subscribe(fakeData =>{
      console.log(fakeData);
      this.sensors = fakeData;
    })
    // this.sensors = this.dataService.getSensors();
    // this.sensors = this.sensors;
  }

  onEditClick(sensor){
    // console.log(sensor.sensor_Name);
    this.sensor = sensor;
  }

  onSubmit(){
    // this.dataService.editSensor(this.sensor);
    //   // this.sensors.unshift(sensor);
    //   console.log(this.sensor);

  }

  // onSubmit(){
  //   this.dataService.editSensor(this.sensor)
  //     for(let i = 0; i < this.sensors.length; i++ ){
  //       if(this.sensors[i].sensor_Name == this.sensor.sensor_Name){
  //         this.sensors.splice(i,1);
  //       }
  //     }
  //     this.sensors.unshift(this.sensor);
  // }

  ngOnInit() {

  }

}
