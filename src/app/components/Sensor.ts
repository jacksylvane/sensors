export interface Sensor {
 sensor_Name: string,
 sensor_Reading_CreateDate: string,
 sensor_Reading_Dec: number,
 sensor_Reading_Int: number,
 sensor_Reading_Txt: string,
 sensor_Reading_UID: string,
 sensor_UID: string,
 isActive: boolean
}
