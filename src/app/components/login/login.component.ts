import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  password: string;
  username: string;

  constructor(private router:Router, private userService:UserService) {

  }

  loginUser(f){
    if (f.invalid) {
      console.log("Form is invalid");
      return;
    }
    const user = {
      username: this.username,
      password: this.password

    }
    //
    console.log(this.username);
    console.log(this.password);

    if(!this.userService.validateLogIn(user)){
      console.log("Please fill in all the fields");
      user.username = this.username;
      user.password = "";
      return false;
    }
    this.userService.validateCredentials(user).then(response =>
      {
        console.log("resp ", response);
        if (!response) {
          console.log("sda");
          user.password = "";
          console.log("Username or Password is incorrect. Try again.");
        }  else {
          console.log("Welcome");
          this.userService.setUserLoggedIn();
          this.router.navigate(['/list']);

          localStorage.setItem('currentUser', 'loggedIn');
        }
      }
    );
  }
  
  ngOnInit() {
    if(localStorage.getItem("currentUser") == "loggedIn"){
      var currentUser = localStorage.getItem("user");
      console.log(currentUser);
      this.userService.setUserLoggedIn();
      this.router.navigate(['/list']);
    }
  }
}
