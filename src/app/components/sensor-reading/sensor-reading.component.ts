import { Component, OnInit, Input } from '@angular/core';
import { Sensor } from '../Sensor';

@Component({
  selector: 'app-sensor-reading',
  templateUrl: './sensor-reading.component.html',
  styleUrls: ['./sensor-reading.component.sass' ]
})
export class SensorReadingComponent implements OnInit {
  @Input('sensor') sensor: Sensor;
  constructor() {
}

/* Returns class dependent on a temperature */
  getTempClass(sensor) {
    if (sensor.sensor_Reading_Dec <= 10) {
      return 'cold';
    }
    if (sensor.sensor_Reading_Dec < 20) {
      return 'medium';
    }
    return 'hot';
  }

/* Returns icon dependent on a temperature */
  getTempIcon(sensor) {
    if (sensor.sensor_Reading_Dec <= 10) {
      return 'fa fa-thermometer-empty';
    }
    if (sensor.sensor_Reading_Dec < 20) {
      return 'fa fa-thermometer-half';
    }
    return 'fa fa-thermometer-full';
  }

  ngOnInit() {
  }

}
